from bash_functions import *
from datetime import datetime
import regex as re
import pickle
import os

py_file_version = '1.2 - multilayer'

functions_version = 'bash 1.1.8'
path = os.getcwd()

#seed = 100

#parameters read from the bash script
folder_name =   (sys.argv)[1]
#lam =          float((sys.argv)[2])

if not os.path.exists(folder_name):
    os.makedirs(folder_name)

    if not folder_name.endswith('/runfiles'): 
        os.makedirs(folder_name + '/runfiles')
        os.makedirs(folder_name + '/sim_output')
        folder_name += '/runfiles'


#######################
#general params

mesh =              'square'
shape =             'circle'

alpha =             180
frame_bool =        False
passive_frame =     False
area =              10*10
half_square_side =  None
h_triangle =        None

if shape == 'circle':
    shape_r = np.sqrt(area/np.pi)

elif shape == 'square':
    shape_r = octagon_act_r(180, area)[0]
else:
    shape_r, half_square_side, h_triangle = octagon_act_r(alpha, area)
    
keep_shape_only = True


#######################
#SIM PARAMS

dt = 0.1
time = 15000

csv_t_save =    250
vtk_t_save =    500

dim_max =       25


#######################
#MATERIAL PARAAMS

a =                0.25
thickness =        a
density =          964 #read from bash
E =                75
k =                (E*a)/2.5
#damping ratio!!
ns =                0.02
dim =              (2*shape_r, 2*shape_r, thickness)


extension_type =    'nematic_circular_pattern'
lam =                0.9
nu_perp =            0.5
nu=                 -1
total_F_ext =       0


#GENERATE MESH
print('Initializing ' + mesh + ' mesh')
balls_df, springs_df = make_square_mesh(dim, a, k, ns, density, shape, keep_shape_only = keep_shape_only, alpha = alpha, half_square_side = half_square_side, h_triangle = h_triangle)

mass_unit = balls_df.iloc[0].mass/balls_df.iloc[0].mass_density

####
### add missing parameters (expected by some further functions)

balls_df['frame_vect1'] = -1
balls_df['frame_vect2'] = -1

if frame_bool == False:
    balls_df['frame'] = 0
else:
    balls_df['frame'] = np.where(balls_df.n_neigh <= 10, 1, 0)
    

#if you want a specific subset of springs to be active, change this!
springs_df['active'] = True

balls_df['ext_fx'] = 0
balls_df['ext_fy'] = 0
balls_df['ext_fz'] = 0

print('Implementing extensions')
if extension_type == 'nematic_circular_pattern':
    springs_df = nematic_extension(balls_df, springs_df, gamma=lam, nu=nu_perp)
    
elif extension_type == 'nematic_square_pattern':
    springs_df = square_nematic_extension(balls_df, springs_df, gamma = lam, nu_perp = nu_perp)

springs_df['l0'] = np.where(springs_df.active, springs_df['l0'], springs_df['l1'])
springs_df['inplane_ext_frac'] = np.where(springs_df.active == 1, springs_df['inplane_ext_frac'], 1)


######################
#save files

print('Generating runfiles')
balls_df['frame'] = balls_df['frame'].astype(int)
save_files_for_cpp(balls_df, springs_df,
                   path + '/' + folder_name,
                   spring_columns = ['l0', 'inplane_ext_frac', 'k', 'viscoelastic_coeff', 'active'],
                   part_columns = ['mass', 'mass_density', 'active', 'frame', 'ext_fx', 'ext_fy', 'ext_fz'])



#save simulation parameters:
if dim_max == 0:
    dim_max = np.max(balls_df[['x', 'y', 'z']].values.flatten()) + 3
nbs_max = max(balls_df.n_neigh)

sim_params = np.array([dt, time, csv_t_save, vtk_t_save, dim_max, nbs_max])
np.savetxt(path + '/' + folder_name + "/sim_params.csv", sim_params, delimiter=" ", newline=" ")

with open(path + '/' + folder_name + "/sim_params.csv", "a") as csvfile:
    csvfile.write(" \n")
    #csvfile.write(folder_name)
    csvfile.write('\ndt' + ' time' + ' csv_t_save' + ' vtk_t_save' + ' dim_max' + ' nbs_max')
    
balls_df.to_csv(path + '/' + folder_name + '/balls_df.csv')
springs_df.to_csv(path + '/' + folder_name + '/springs_df.csv')


with open(f'{path}/{folder_name}/balls_df.pickle', 'wb') as handle:
    pickle.dump(balls_df, handle)
    
with open(f'{path}/{folder_name}/springs_df.pickle', 'wb') as handle:
    pickle.dump(springs_df, handle)


with open(path + '/' + folder_name + "/Python_params.txt", "w") as text_file:
    
    print('File generated on: ' + str(datetime.now()) + '\n', file = text_file)
    
    print('\n~~~~~simulation dimensions~~~~~~\n', file = text_file)
    #print('distance unit: 1mm (10e-3)m\n'
    #        + 'mass unit: 10e-6 kg\n'
    #        + 'time unit: 5*10e-4s (1s = 2000 time units)\n'
    #        + 'force unit: 0.004N\n'
    #        + 'E=125 corresponds to 500kPa\n'
    #        + '~~~~~~~~~~~~~~~~~~~~~~~~~\n\n'
    #        ,file = text_file)
    
    
    #for name in param_names:
    #    print(name + ': ' + str(globals()[name]), file=text_file)
    
    if re.match(r'nematic', extension_type):
        print('nem. springs fold change: ' + str(nem_direction(lam)), file=text_file)
        print('perp. springs fold change: ' + str(round(perp_direction(lam, nu_perp), 3)), file=text_file)
        print('predicted angle: ', np.round(np.degrees(np.arcsin(lam**(1+nu_perp))), 3), file=text_file)

    
    print('dimensions: ' +
          str((np.round(min(balls_df.x), 3), np.round(min(balls_df.y), 3), np.round(min(balls_df.z), 3))) +
          ', ' +
          str((np.round(max(balls_df.x), 3), np.round(max(balls_df.y), 3), np.round(max(balls_df.z), 3))),
              file=text_file)
    print('number of balls: ' + str(len(balls_df)), file=text_file)
    print('number of springs: ' + str(len(springs_df)), file=text_file)
    print('proportion of active balls: ', np.round(len(balls_df[balls_df.active == 1])/len(balls_df), 3), file=text_file)
    print('max. number of neighbours: ', max(balls_df.n_neigh), file=text_file)
