#!/bin/bash


#this is a bash file for running the minimal example simulation

folder='test'

mkdir "simulations"

#generate directories for runfiles and simulation output
dir="simulations/${folder}"
dir_makefiles="${dir}/runfiles"
dir_sim_output="${dir}/sim_output"

mkdir $dir
mkdir $dir_makefiles
mkdir $dir_sim_output

#export OPENBLAS_NUM_THREADS=1

#generate runfiles in python
python3 generate_csvs.py $dir_makefiles 

#run openfpm
#cluster:
source /home/szalapak/openfpm_vars_newint
module load gcc/8.3.0
module load openmpi/4.0.0

#mac:
#source /Users/szalapak/openfpm_vars

#make
example_odeint $dir

#this bash can be adapted for running param sweeps by passing SBATCH parameters

#this can be either done by generating a csv with param values, 
#having bash read the appropriate line depending on task ID
#create folder in the simulations folder 
#generate runfiles using the passed params
#and run the simulation from the appropriate folder
