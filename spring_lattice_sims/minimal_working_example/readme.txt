This is a minimal example of a material deformation simulation
A disc with a spontaneous strain in the pattern of concentric circles is allowed to relax (simple gradient descent to minimise energy), which results in a shape change from a flat disc into a cone 


To run simulation from the terminal:
bash run_sim.sh


Bash generates simulation folder (if it doesn't exist)
Inside it, it creates an individual folder corresponding to a single simulation
For each simulation, runfiles (coordinates of particles, neighbourhood list, material properties of the disc etc) are generated using python and saved to the appropriate folder. From there, the run files are read by cpp which simulates the shape change. 
The simulation generates .csv & .vtk files 

This code can be extended to change how run files are generated, add additional events to the simulation or extend the single simulation into a parameter sweep. 
