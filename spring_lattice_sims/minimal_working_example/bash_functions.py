import pandas as pd
import numpy as np
import math
import sys
import random
from scipy.integrate import solve_ivp
import os
import networkx as nx
import csv

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.path as mpath
import matplotlib.colors as mcolors
from matplotlib import collections  as mc
import matplotlib.cm as cm

from scipy.spatial import Delaunay
from scipy.spatial import distance_matrix
from scipy.spatial import ConvexHull

from time import perf_counter

'''
version 1.1.8-performance improvements-stacking, changed save_csv_files()
'''

def save_files_for_cpp(balls_df, springs_df, path, spring_columns, part_columns):
    
    """The function generates a ball.csv file with particle ID,
    position (x,y,z), number of neighbours + attributes from part_columns
    
    
    as well as files containing attributes of springs -
    a separate .csv file for each of the chosen spring_columns
    
    For ith row in the .csv, the values give info on selected attribute
    of all neighbours of the ith particle
    (eg in neighbours.csv, jth value in ith row corresponds
    to the ID of the jth neighbour of ith particle)
    the attributes are saved in the same order as neighbours
    IDs are saved in the list in the 'neighbours' column of balls_df dataframe
    This is then imported into cpp for each simulation
    """
    #make neighbour_lengths dataframe and populate the 'neighbours' column with a list of neighbours for each ball
    neigh_lengths = pd.DataFrame(index=balls_df['ID'], columns=['neighbours'] + spring_columns)

    nbs = [np.append(springs_df.loc[balls_df.loc[ball].spring2].ball1.values, springs_df.loc[balls_df.loc[ball].spring1].ball2.values) for ball in balls_df.ID]
    neigh_lengths['neighbours'] = nbs

    for column in spring_columns:
        vals = [np.append(springs_df.loc[balls_df.loc[ball].spring2][column].values, springs_df.loc[balls_df.loc[ball].spring1][column].values) for ball in balls_df.ID]

        neigh_lengths[column] = vals


    for column in ['neighbours'] + spring_columns:
        neighb_list = neigh_lengths[column].values.tolist()

        if column != 'neighbours': name = 'neigh_' + column
        else: name = column

        with open(path + '/' + name + '.csv', "w", newline="") as f:
            writer = csv.writer(f, delimiter=' ')
            writer.writerows(neighb_list)
            
    
    
    balls_df['n_neigh'] = balls_df['neighbours'].str.len()
    
    #balls.csv file contains particle attributes - global ID, position, number of neighbours and
    #the indication whether the particle is active or not
    balls=balls_df[['ID','x','y','z','n_neigh']+part_columns].values
    np.savetxt(path + "/balls.csv", balls, delimiter=" ")

def octagon_act_r(alpha, tot_area, act_prop = 1):
    
    beta = 270 - alpha
    gamma = np.abs((beta-90)/2)
    gamma_rad = math.radians(gamma)

    if alpha <= 180:
        a = np.sqrt(act_prop*tot_area/(4*(1+np.tan(gamma_rad))))
        h = a*np.tan(gamma_rad)
    else: 
        a = np.sqrt(tot_area)*np.sqrt(np.tan(gamma_rad) + 1)/(2*np.sqrt(2*np.tan(gamma_rad) + 1))
        h = a*(np.tan(gamma_rad)/(1 + np.tan(gamma_rad)))
        #area = (2*act_r)**2 - 4*act_r*h
    
    act_r = a+h
    half_square_side = a
    h_triangle = h
    return(act_r, half_square_side, h_triangle)

def octogonal_footprint(balls_df, r, alpha, sq_c, h):
    
    if alpha > 180:
        sq_c = sq_c + h
        h = -1*h
    
    oct_vert = [[0, sq_c+h],
             [sq_c, sq_c],
             [sq_c+h, 0],
             [sq_c, -sq_c],
             [0, -(sq_c+h)],
             [-sq_c, -sq_c],
             [-(sq_c+h), 0],
            [-sq_c, sq_c]]
    
    oct_vert = np.asarray(oct_vert)
    
    oct_path = mpath.Path(np.append(oct_vert, [oct_vert[0]], axis = 0), closed = True)
    
    balls_df['active'] = oct_path.contains_points(balls_df[['x', 'y']])
    
    return(balls_df, oct_vert)
    
def make_square_mesh(dim, a, k, ns, density, shape,
                    keep_shape_only = True, alpha = None, half_square_side = None, h_triangle = None):
    
    t0 = perf_counter()
    start = perf_counter()

    xs = np.arange(-a*round(dim[0]/a)/2, a*round(dim[0]/a)/2 + a/5, a)
    ys = np.arange(-a*round(dim[1]/a)/2, a*round(dim[1]/a)/2 + a/5, a)

    xs, ys = np.meshgrid(xs, ys)

    positions = np.vstack([xs.ravel(), ys.ravel()])

    balls_colnames=['ID', 'x', 'y', 'z', 'neighbours', 'spring1', 'spring2', 'mass']
    balls=pd.DataFrame(0, index=range(0), columns=range(len(balls_colnames)))
    balls.columns=balls_colnames
    balls.index = range(balls.shape[0])


    balls[['x', 'y']] = np.vstack([positions.T for i in range(int(dim[2]/a) + 1)])

    balls.z = np.concatenate([(np.repeat(i, len(positions.T))) for i in np.arange(0, dim[2] + a/5, a)])

    print(f'\tgenerated {len(balls)} particles in {perf_counter() - start:.2}s')


    start = perf_counter()
    ###define shape of footprint
    if shape == 'circle':
        balls['active'] = np.where(np.sqrt(balls.x**2 + balls.y**2) <= dim[0]/2, True, False)

    elif shape == 'octagon':
        balls, oct_vert = octogonal_footprint(balls, r = dim[0]/2, alpha = alpha, sq_c = half_square_side, h = h_triangle)

    else:
        balls['active'] = True

    if keep_shape_only:
        balls = balls[balls.active]
        balls.reset_index(drop=True, inplace=True)

    print(f'\tchanged mesh shape to {shape} with {len(balls)} particles in {perf_counter() - start:.2}s')    

    #####assign neighbours
    start = perf_counter()

    balls.ID = balls.index.values
    balls['neighbours'] = balls['neighbours'].astype('object')

    #get distances between all particles and check where those distances are less than sqrt(2)*cube side
    #ids contains particle id and the corresponding value in nbs contains its neighbour
    dists = distance_matrix(balls[['x', 'y', 'z']], balls[['x', 'y', 'z']])

    ids, nbs = np.where((dists > 0) & (dists < np.sqrt(2)*a + 0.01))

    split_loc, = np.where(ids[:-1] != ids[1:])
    split_loc += 1

    balls['neighbours'] = np.split(nbs, split_loc)

    n_nbs = np.append(np.array(split_loc[0]), split_loc[1:] - split_loc[:-1])
    n_nbs = np.append(n_nbs, len(ids) - split_loc[-1])

    balls['n_neigh'] = n_nbs

    print(f'\tassigned neighbours in {perf_counter() - start :.2}s') 

    ###generate springs df:
    start = perf_counter()

    ball1 = ids[nbs > ids]
    ball2 = nbs[nbs > ids]

    springs = pd.DataFrame(np.hstack((balls.loc[ball1][['x', 'y', 'z']].values, balls.loc[ball2][['x', 'y', 'z']].values)),
                           columns = ['x1', 'y1', 'z1', 'x2','y2','z2'])

    springs['ball1'] = ball1
    springs['ball2'] = ball2

    springs['l1'] = dists[ball1,ball2]
    
    springs['l0'] = springs['l1']

    #slow but more logical way:
    #balls['spring1'] = balls.apply(lambda row: np.where(ball1 == row.ID)[0], axis = 1)
    #balls['spring2'] = balls.apply(lambda row: np.where(ball2 == row.ID)[0], axis = 1)

    #fastt
    split_b1, = np.where(ball1[:-1] != ball1[1:])
    split_b1 += 1

    balls['spring1'] = [np.array([]) for i in balls.ID]
    balls.loc[np.unique(ball1), 'spring1'] = np.split(springs.index.values, split_b1)

    ind_sorted = np.argsort(ball2)
    sorted_b2 = ball2[ind_sorted]

    split_b2, = np.where(sorted_b2[:-1] != sorted_b2[1:])
    split_b2 += 1

    balls['spring2'] = [np.array([]) for i in balls.ID]
    balls.loc[np.unique(sorted_b2), 'spring2'] = np.split(ind_sorted, split_b2)

    print(f'\tgenerated {len(springs)} springs in {perf_counter() - start :.2}s')
    
    
    start = perf_counter()
    

    balls, springs = assign_k_mass_density(balls, springs, dim, k, ns, density, a, shape)
    
    springs['ID'] = springs.index

        
    print(f'\tmasses & k assigned in {perf_counter() - start :.2}s')
    
    print(f'generated parts, springs in {perf_counter() - t0:.2}s')  
    
    return(balls, springs)

def assign_k_mass_density(balls, springs, dim, k, ns, density, a, shape):
    
    t0 = perf_counter()
    start = perf_counter()

    #### diagonal vs edge springs
    ind_de1 = (springs.x1 != springs.x2) & (springs.y1 != springs.y2)
    ind_de2 = (springs.x1 != springs.x2) & (springs.z1 != springs.z2)
    ind_de3 = (springs.y1 != springs.y2) & (springs.z1 != springs.z2)

    ind_diag_edge = ind_de1 | ind_de2 | ind_de3

    springs['diag_edge'] = np.where(ind_diag_edge, 'diagonal', 'edge')

    springs['diag_edge'] = np.where((springs.y1 == springs.y2) & (springs.x1 == springs.x2), 'vertical', springs['diag_edge'])
    springs['diag_edge'] = np.where((springs.z1 == springs.z2) & (springs['diag_edge'] != 'diagonal'), 'horizontal', springs['diag_edge'])

    #### mass density
    balls['mass_density'] = 1

    balls['mass_density'] = np.where(balls.n_neigh <= 13, 0.5, balls['mass_density'])
    balls['mass_density'] = np.where(balls.n_neigh <= 9, 0.25, balls['mass_density'])
    balls['mass_density'] = np.where(balls.n_neigh <= 6, 0.125, balls['mass_density'])

    springs['in_out'] = np.where((balls.iloc[springs.ball1].n_neigh <= 13 ).values & (balls.iloc[springs.ball2].n_neigh <= 13).values, 'out', 'in')

    print(f'\tassigned mass density in {perf_counter() - start :.2}s')

    if (shape == 'square') & (len(balls) < 5000):
        ks = assign_square_ks_old(springs, balls, k, dim, a)
        frame = None
    else: 
        #in general non-square cases don't work very well - there should be some particles that have other fractions of masses (0.75, etc)
        ks, frame = quick_and_dirty_k(springs, balls, k, dim, a)
    
    springs['k'] = ks
    
    if not (frame is None):
        balls['frame'] = frame

    print(f'\tall ks assigned in {perf_counter() - t0 :.2}s')
    
    
    ### mass
    volume = dim[0]*dim[1]*dim[2]

    mass_unit = volume*density/np.sum(balls['mass_density'])

    balls['mass'] = balls['mass_density']*mass_unit

    ### viscoelastic coeff
    
    ####viscoelastic coefficient now calculated using damping ratio (ns = visc_coeff/(2*sqrt(mk))) - 
    #for the damping ratio between meshes to remain constant between different springs in a mesh 
    #and between different meshes 

    #this has to be further corrected when changing mesh density - somehow the appropriate scaling constant 
    #is proportional to the ratio of cube side length (so when a mesh size is increased from 0.25 to 0.5, 
    #viscoelastic constant needs to be doubled)
    #this is done here in relation to the standard mesh of 0.25
    #shouldn't this be square?? - check tomorrow!
    
    springs['viscoelastic_coeff'] = (a/0.25) * 2* ns * np.sqrt(springs.k.values * (balls.loc[springs.ball1].mass.values + balls.loc[springs.ball2].mass.values))
    
    #springs['viscoelastic_coeff'] = springs.apply(lambda row : ((a/0.25)**2) * ns*2*np.sqrt(row.k*(np.sum(balls.iloc[row[['ball1', 'ball2']].values].mass)/2)), axis = 1)
    
    return(balls, springs)
    
def square_nematic_extension(balls_df, springs_df, gamma = 0.6, nu_perp = 0.3569, nu = -1, thin = False):
    
    #identify positions of balls within the square
    balls_df['pos'] = np.where((balls_df.x > -balls_df.y) & (balls_df.x < balls_df.y), 'h', 'v')
    balls_df.loc[(balls_df.x < -balls_df.y) & (balls_df.x > balls_df.y), 'pos'] = 'h'
    balls_df.loc[(abs(balls_df.x + balls_df.y) < 0.1) | (abs(balls_df.x - balls_df.y) < 0.1), 'pos'] = 'e'
    
    #find springs connecting balls that are horizontal, vertical and at the edges
    h_bool = (balls_df.iloc[springs_df.ball1].pos == 'h').values & (balls_df.iloc[springs_df.ball2].pos == 'h').values
    eh_bool = (balls_df.iloc[springs_df.ball1].pos == 'e').values & (balls_df.iloc[springs_df.ball2].pos == 'h').values
    he_bool = (balls_df.iloc[springs_df.ball1].pos == 'h').values & (balls_df.iloc[springs_df.ball2].pos == 'e').values
    h_bool = (eh_bool | he_bool | h_bool)


    v_bool = (balls_df.iloc[springs_df.ball1].pos == 'v').values & (balls_df.iloc[springs_df.ball2].pos == 'v').values
    ve_bool = (balls_df.iloc[springs_df.ball1].pos == 'v').values & (balls_df.iloc[springs_df.ball2].pos == 'e').values
    ev_bool = (balls_df.iloc[springs_df.ball1].pos == 'e').values & (balls_df.iloc[springs_df.ball2].pos == 'v').values
    v_bool = (v_bool | ve_bool | ev_bool)
    
    
    ####extension
    balls=balls_df[['x','y','z']].values
    springs=springs_df[['l0','l1','ball1','ball2','k']].values
    #spring_types=springs_df['type'].values

    #v holds vectors for each spring
    v=balls[springs[:,2].astype(int)]-balls[springs[:,3].astype(int)]
    #only interested in x and y coordinates, as in the end we only consider in-plane springs
    v = v[:,0:2]
    
    nem = nem_direction(gamma)
    perp = perp_direction(gamma, nu_perp)
    
    #calculate the reshaped vectors
    #for areas where nematic field is vertical:
    #the vertical component of each vector (v*[0,1]) gets extended/contracted according to nematic extension/contraction
    #while the horizontal component (v*[1,0]) gets extended/contracted according to the perpendicular extension/contraction
    v_vertical_nematic = v*[1,0]*perp + v*[0,1]*nem
    v_horizontal_nematic = v*[1,0]*nem + v*[0,1]*perp

    #calculate the lengths of resulting vectors
    vert_nem_lo = np.sqrt(v_vertical_nematic[:,0]**2 + v_vertical_nematic[:,1]**2)
    hor_nem_lo = np.sqrt(v_horizontal_nematic[:,0]**2 + v_horizontal_nematic[:,1]**2)
    
    h_indices=np.where(h_bool)
    v_indices=np.where(v_bool)

    #update the lengths of the
    springs[h_indices,0]= hor_nem_lo[h_indices]
    springs[v_indices,0]= vert_nem_lo[v_indices]


    balls_df[['x','y','z']]=balls
    springs_df['inplane_ext']=springs[:,0]

    springs_df['l0'] = np.sqrt(np.square(springs_df.inplane_ext)+np.square(springs_df.z1-springs_df.z2))
    
    return(springs_df)
    
    
    
def radial_extension(x,y,gamma=0.6,nu=0.3569):
    return(gamma**(-nu))

def tangential_extension(x,y,gamma=0.6,nu=-1):
    return(gamma**(-nu))


def perp_direction(gamma=0.6,nu=0.3569):
    return(gamma**(-nu))

def nem_direction(gamma=0.6):
    return(gamma)
    
    

def nematic_extension(balls_df, springs_df, gamma=0.6,nu=0.3569, thin = False):
    #for a good cone, keep gamma=0.6 and nu=0.3569
    #for the best anticone I have till now, keep gamma=1.2 and nu=0.81705
    
    #print(gamma, nu)
    
    vert_len = springs_df[springs_df.diag_edge == 'vertical'].l1.iloc[0]

    #balls=balls_df[['x','y','z']].values
    balls=balls_df[['x','y']].values
    springs=springs_df[['l0','l1','ball1','ball2','k']].values
    #spring_types=springs_df['type'].values

    v=balls[springs[:,2].astype(int)]-balls[springs[:,3].astype(int)]
    v_mod=np.sqrt(np.sum(v**2,axis=1).astype(np.float64))
    v_mod=np.where(v_mod == 0, vert_len, v_mod)
    springs[:,1]=v_mod
    v_cap=v/v_mod[:,None]
    #spring midpoints
    mid_points=(balls[springs[:,2].astype(int)]+balls[springs[:,3].astype(int)])/2
    #mid_points[:,2]=0
    mid_points_mod=np.sqrt(np.sum(mid_points**2,axis=1).astype(np.float64))
    mid_points_mod[np.where(mid_points_mod==0)]=1 #hacky way to handle those points for which modulus of vector is zero on putting z=0
    radial_directions=mid_points/mid_points_mod[:,None]

    rad_ext_array=radial_extension(mid_points[:,0],mid_points[:,1],gamma=gamma,nu=nu) #for a cone, uncomment this line
    #print(rad_ext_array)
    tang_ext_array=tangential_extension(mid_points[:,0],mid_points[:,1],gamma=gamma) #for a cone, uncomment this line
    #print(tang_ext_array)

    #rad_ext_array=radial_extension(mid_points[:,0],mid_points[:,1],gamma=gamma) #for an anticone, uncomment this line
    #tang_ext_array=tangential_extension(mid_points[:,0],mid_points[:,1],gamma=gamma,nu=nu) #for an anticone, uncomment this line

    #vector component in the radial direcion - gets extended/contracted by whatever is the radial extension
    cos_theta=np.absolute((radial_directions*v_cap).sum(axis=1))
    #vector component in the tangential direction - gets extended/contracted by whatever is the tangential extension
    sin_theta=np.absolute(np.sqrt(np.abs((1-cos_theta**2)).astype(np.float64)))

    if(sum(np.isnan(sin_theta)) > 0):
        print(np.where(np.isnan(sin_theta)))
        print(cos_theta[np.isnan(sin_theta)])
        print(1-cos_theta[np.isnan(sin_theta)]**2)

    #desired extension is calculated as the length of vector with cos_theta and sin_theta components
    ext=np.sqrt((cos_theta*rad_ext_array)**2+(sin_theta*tang_ext_array)**2)

    #only the inplane extensions calculated
    springs_df['inplane_ext'] = ext*np.sqrt(np.sum(v**2,axis=1).astype(np.float64))
    springs_df['inplane_ext_frac'] = np.where(springs_df['inplane_ext'] == 0, 1, ext)
    
    #ext=ext+1
    #ext=ext[:,None]
    #spring length multiplied by extension
    #springs[:,0]=springs[:,0]*ext[:,0]

    #balls_df[['x','y','z']]=balls

    
    

    #diagonal spring length is calculated by taking the inplane extension component and its height
    #easier than the update_diagonal_lengths function used previously
    springs_df['l0'] = np.sqrt(np.square(springs_df.inplane_ext)+np.square(springs_df.z1-springs_df.z2))

    
    return(springs_df)

def isBetween(a, b, c, epsilon = 1e-6):
    crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y)

    # compare versus epsilon for floating point values, or != 0 if using integers
    if abs(crossproduct) > epsilon:
        return False

    dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y)
    if dotproduct < 0:
        return False

    squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y)
    if dotproduct > squaredlengthba:
        return False

    return True


def stack_two_layers(base_balls_df, base_springs_df, l1_balls_df, l1_springs_df):
    
    a2 = np.min(l1_springs_df.l1)
    #attempts at speeding up: 
    t_0 = perf_counter()

    merged_balls = base_balls_df.copy(deep = True)
    l1_updated_balls = l1_balls_df.copy(deep = True)
    updated_springs = l1_springs_df.copy(deep = True)

    N_base_balls = len(base_balls_df)
    print(N_base_balls)
    N_base_springs = len(base_springs_df)

    z_interface = max(base_balls_df.z)

    coplanar_base_springs =  base_springs_df[np.all(base_springs_df[['z1', 'z2']] == z_interface, axis = 1)].copy()
    coplanar_l1_springs =  l1_springs_df[np.all(l1_springs_df[['z1', 'z2']] == z_interface, axis = 1)].copy()
    #get coords of coplanar springs
    b1_coords = coplanar_l1_springs[['x1', 'y1']].set_axis(['x', 'y'], axis=1, inplace=False)
    b2_coords = coplanar_l1_springs[['x2', 'y2']].set_axis(['x', 'y'], axis=1, inplace=False)

    replacement_dict = {}

    base_l1 = l1_balls_df.z == np.min(l1_balls_df.z)
    top_base = base_balls_df.z == np.max(base_balls_df.z)
    
    l1_inds, base_ind = np.where(distance_matrix(l1_balls_df[base_l1][['x', 'y', 'z']], base_balls_df[top_base][['x', 'y', 'z']]) < 0.1*a2)

    matched_ind = base_balls_df[top_base].iloc[base_ind].ID.values
    duplicated_inds = l1_balls_df[base_l1].iloc[l1_inds].ID.values
    
    updated_duplicate_ind = duplicated_inds + N_base_balls
    
    print(f'\tfound duplicate particles at {len(duplicated_inds)} locations')
    
    start = perf_counter()

    #get coordinates of springs that are attached to duplicate particles 
    #need to do it before we start adding extra springs
    springs_to_replace = np.unique(np.append(np.concatenate(merged_balls.loc[matched_ind].spring1.values), np.concatenate(merged_balls.loc[matched_ind].spring2.values)))
    springs_to_replace = springs_to_replace[(base_springs_df.loc[springs_to_replace].z1 == base_springs_df.loc[springs_to_replace].z2)]


    replacements = dict(zip(updated_duplicate_ind, matched_ind))
    replacer = replacements.get

    mass_updated = pd.Series(base_balls_df.loc[matched_ind].mass.values + l1_updated_balls.loc[duplicated_inds].mass.values, index = matched_ind)

    merged_balls['mass'] =  merged_balls.loc[~merged_balls.index.isin(matched_ind)].mass.append(mass_updated)

    if 'ext_fx' in merged_balls.columns: 

        forces_updated = pd.DataFrame(base_balls_df.loc[matched_ind][['ext_fx', 'ext_fy', 'ext_fz']].values + 
                                      l1_updated_balls.loc[duplicated_inds][['ext_fx', 'ext_fy', 'ext_fz']].values, 
                                     columns = ['ext_fx', 'ext_fy', 'ext_fz'], index = matched_ind) 

        merged_balls[['ext_fx', 'ext_fy', 'ext_fz']] = merged_balls.loc[~merged_balls.index.isin(matched_ind)][['ext_fx', 'ext_fy', 'ext_fz']].append(forces_updated)

    print(f'\tmerged {2*len(duplicated_inds)} duplicate particles in {perf_counter() - start:.2}s')

    start = perf_counter()

    b1_b1 = distance_matrix(base_springs_df.loc[springs_to_replace][['x1', 'y1']], coplanar_l1_springs[['x1', 'y1']])
    b2_b2 = distance_matrix(base_springs_df.loc[springs_to_replace][['x2', 'y2']], coplanar_l1_springs[['x2', 'y2']])

    b1_b2 = distance_matrix(base_springs_df.loc[springs_to_replace][['x1', 'y1']], coplanar_l1_springs[['x2', 'y2']])
    b2_b1 = distance_matrix(base_springs_df.loc[springs_to_replace][['x2', 'y2']], coplanar_l1_springs[['x1', 'y1']])

    long_ind, dummy_ind = np.where(np.abs((b1_b1 + b2_b2) - base_springs_df.loc[springs_to_replace].l1.values[:, None] + coplanar_l1_springs.l1[None, :]) < 1e-4)
    long_ind1, dummy_ind1 = np.where(np.abs((b1_b2 + b2_b1) - base_springs_df.loc[springs_to_replace].l1.values[:, None] + coplanar_l1_springs.l1[None, :]) < 1e-4)

    if len(long_ind1) > 0:
        print('springs in another list!')

    #assumes l1 is finer than base!
    N_dummy = (np.min(base_springs_df.l1)/np.min(l1_springs_df.l1))

    replaced_springs = base_springs_df.loc[springs_to_replace[long_ind]]
    new_fine_springs = coplanar_l1_springs.iloc[dummy_ind].copy()

    new_fine_springs.index = np.arange(len(new_fine_springs)) + len(l1_springs_df) + N_base_springs
    #print(new_fine_springs.index)

    new_fine_springs.ID = np.arange(len(new_fine_springs)) + len(l1_springs_df) + N_base_springs

    new_fine_springs.k = N_dummy/replaced_springs.k.values

    new_fine_springs.l1 = replaced_springs.l1.values/N_dummy
    new_fine_springs.l0 = replaced_springs.l0.values/N_dummy

    new_fine_springs.inplane_ext_frac = replaced_springs.inplane_ext_frac.values

    print(f'\tgenerated {len(new_fine_springs)} dummy springs in {perf_counter() - start:.2}s')

    #need to remove long springs and so the balls they connected from neighbours lists
    #maybe update neighbours & connected springs the way you did it to begin with?

    start = perf_counter()
    #maybe I should have updatd IDs at the beginning? 
    #remove long springs from the base_spring_df
    updated_base_springs = base_springs_df[~base_springs_df.index.isin(springs_to_replace)].copy(deep = True)

    #update the IDs of particles in the new
    non_duplicated = l1_updated_balls[~l1_updated_balls.index.isin(duplicated_inds)]
    non_duplicated.index = non_duplicated.index + N_base_balls

    updated_springs.ball1 = updated_springs.ball1 + N_base_balls
    updated_springs.ball2 = updated_springs.ball2 + N_base_balls
    updated_springs.index = updated_springs.index + N_base_springs

    new_fine_springs.ball1 = new_fine_springs.ball1 + N_base_balls
    new_fine_springs.ball2 = new_fine_springs.ball2 + N_base_balls

    new_springs = pd.concat((updated_springs, new_fine_springs))
    #non_duplicated.neighbours = non_duplicated.apply(lambda row: list(np.array(row.neighbours) + N_base_balls), axis = 1)
    #merged_balls.neighbours = merged_balls.apply(lambda row: [replacer(n, n) for n in row.neighbours], axis = 1)
    new_springs.ball1 = [replacer(n, n) for n in new_springs.ball1.values]
    new_springs.ball2 = [replacer(n, n) for n in new_springs.ball2.values]

    springs_df = updated_base_springs.append(new_springs)
    balls_df = merged_balls.append(non_duplicated)

    print(f'\tmerged particle and spring arrays in {perf_counter() - start:.2}s')


    start = perf_counter()

    s1 = [springs_df.index.values[np.where(springs_df.ball1 == idd)[0]] for idd in balls_df.index]
    s2 = [springs_df.index.values[np.where(springs_df.ball2 == idd)[0]] for idd in balls_df.index]

    balls_df.spring1 = s1
    balls_df.spring2 = s2

    all_sp = [np.append(s1_val, s2_val) for s1_val, s2_val in zip(s1, s2)]

    #nbs = [updated_springs.loc[val][['ball1', 'ball2']].values[updated_springs.loc[val][['ball1', 'ball2']].values != ind] for ind, val in zip(l1_balls_df.index, all_sp)]
    nbs = [np.append(springs_df.loc[s2_val].ball1.values, springs_df.loc[s1_val].ball2.values)  for s1_val, s2_val in zip(s1, s2)]

    balls_df.neighbours = nbs
    balls_df.n_neigh = [len(nb) for nb in nbs]
    
    balls_df.ID = balls_df.index.values
    springs_df.ID = springs_df.index.values

    print(f'\tupdated neighbourhood lists in {perf_counter() - start:.2}s')
    
    print(f'stacked meshes in {perf_counter() - t_0:.2}s')
    
    return(balls_df, springs_df)


