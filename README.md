This folder contains several examples of code I have written for different projects in the past two years. 

In order of newest -> oldest: 

- CF_plotting contains a Jupyter notebook I used to generate figures for a conference poster. I have used this in the past two weeks, though some functions were developed in the previous months. The data comes from simulations of tissue fold formation during fruit fly development. The simulations themselves were developed together with another PhD student so I have not included them here.

- DNA_images contains Jupiter notebooks from PhD course in November '21. I was working on analysing movies of how DNA molecules pinned to a slide fluctuated over time. I wanted to quantify those fluctuations to investigate physical properties of the DNA strands and see whether binding of single protein molecules (SMs) influenced them. I included two Jupiter notebooks - one was used to detect DNA molecules and SMs in individual images while the other linked the DNAs and molecules across frames to look at how they changes over time. 

- spring_lattice_sims contains several files that I use to run simulations of material deformations. Here, I model materials as a collection of particles and springs and see how different patterns of stresses within the material can lead to shape changes. This code is the oldest (big part of it was developed right at the beginning of my PhD) and probably the most messy. 


